using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Immutability
{
	public class Test
	{
		public Test()
		{
		}
		public Test(Type[] typesInAssembliesToTest)
			: this(typesInAssembliesToTest.Select(t => Assembly.GetAssembly(t)).ToArray())
		{
		}
		public Test(Assembly[] assembliesToTest)
		{
			_assembliesToTest.AddRange(assembliesToTest.ToList());
		}

		private List<Assembly> _assembliesToTest = new List<Assembly>(){
			System.Reflection.Assembly.GetExecutingAssembly(),
		};
		private IEnumerable<Assembly> AssembliesToTest => _assembliesToTest;

		// It's particularly important that 'struct' types are immutable.
		// for a short discussion, see http://blogs.msdn.com/jaybaz_ms/archive/2004/06/10/153023.aspx
		public void EnsureStructsAreImmutableTest()
		{
			var mutableStructs = from type in AssembliesToTest.GetTypes()
				where IsMutableStruct(type)
				select type;

			if (mutableStructs.Any())
			{
				var message = $"'{mutableStructs.First().FullName}' is a value type, but was not marked with the [Immutable] attribute";
				throw new ImmutableAttribute.ImmutableFailureException(mutableStructs.First(), message);
			}
		}

		// ensure that any type marked [Immutable] has fields that are all immutable
		public void EnsureImmutableTypeFieldsAreMarkedImmutableTest()
		{
			var whiteList = new Type[] {
				typeof(IEnumerable<>),
					typeof(IList<>),
					typeof(IReadOnlyCollection<>),
					typeof(ICollection<>),
					typeof(IDictionary<,>),
			};
			// try
			// {
			ImmutableAttribute.VerifyTypesAreImmutable(AssembliesToTest, whiteList);
			// }
			// catch (ImmutableAttribute.ImmutableFailureException ex)
			// {
			//     Console.Write(FormatExceptionForAssert(ex));
			//     Assert.False(true, $"'{ex.Type.Name}' failed the immutability test. " + Environment.NewLine + ex.Message);
			// }
		}

		internal static bool IsMutableStruct(Type type)
		{
			if (!type.IsValueType) return false;
			if (type.IsEnum) return false;
			if (type.IsSpecialName) return false;
			if (type.Name.StartsWith("__")) return false;
			// if (type.IsDefined(typeof(CompilerGeneratedAttribute), false)) return false;
			if (ReflectionHelper.TypeHasAttribute<ImmutableAttribute>(type)) return false;
			if (!type.IsInterface) return false;
			return true;
		}

		internal static string FormatExceptionForAssert(Exception ex)
		{
			var sb = new StringBuilder();

			string indent = "";

			for (; ex != null; ex = ex.InnerException)
			{
				sb.Append(indent);
				sb.AppendLine(ex.Message);

				indent = indent + "    ";
			}

			return sb.ToString();
		}
	}
}
