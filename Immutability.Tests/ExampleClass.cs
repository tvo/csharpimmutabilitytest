namespace Immutability.Tests
{
    [Immutable]
    public class ExampleClass
    {
        public int Id { get; }
    }
}
