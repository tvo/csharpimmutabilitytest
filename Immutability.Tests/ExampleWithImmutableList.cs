using System.Collections.Immutable;

namespace Immutability.Tests
{
	[Immutable]
	public class ExampleWithImmutableList
	{
		public ImmutableList<int> MyList { get; }
		public ImmutableArray<int> MyArray { get; }
		public ImmutableDictionary<int, int> MyDictionary { get; }
		public ImmutableHashSet<int> MyHashSet { get; }
		public ImmutableQueue<int> MyQueue { get; }
		public ImmutableSortedDictionary<int, int> MySortedDictionary { get; }
		public ImmutableSortedSet<int> MySortedSet { get; }
		public ImmutableStack<int> MyStack { get; }

	}
}
