using System;
using System.Reflection;
using Xunit;

namespace Immutability.Tests
{
    /// Add these two tests to your test project. One of them looks for immutable structs, the other looks for classes.
    public class Tests
    {
        /// This test verifies immutable structs
        [Fact]
        public void EnsureStructsAreImmutableTest()
        {
            Type t = typeof(ExampleClass);
            var p = new Assembly[] { t.Assembly };
            new Immutability.Test(p).EnsureStructsAreImmutableTest();
        }

        /// This test verifies the immutability of classes.
        [Fact]
        public void EnsureImmutableTypeFieldsAreMarkedImmutableTest()
        {
            Type t = typeof(ExampleClass);
            var p = new Assembly[] { t.Assembly };
            new Immutability.Test(p).EnsureImmutableTypeFieldsAreMarkedImmutableTest();
        }
    }
}
